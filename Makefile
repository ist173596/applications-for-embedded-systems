COMPONENT=NuclearPlantSystemAppC
BUILD_EXTRA_DEPS = AseRootMsg.py AseRootMsg.class

AseRootMsg.py: AseRootMsg.h
	mig python -target=$(PLATFORM) $(CFLAGS) -python-classname=AseRootMsg AseRootMsg.h AseRootMsg -o $@

AseRootMsg.java: AseRootMsg.h
	mig java -target=$(PLATFORM) $(CFLAGS) -java-classname=AseRootMsg AseRootMsg.h AseRootMsg -o $@

AseRootMsg.class: AseRootMsg.java
	javac AseRootMsg.java

include $(MAKERULES)