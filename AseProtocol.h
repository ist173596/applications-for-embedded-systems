#ifndef ASE_PROTOCOL_H
#define ASE_PROTOCOL_H

#define MAX_TEMP_ALERT  50
#define MAX_SMOKE_ALERT 1
#define MAX_RAD_ALERT   50

typedef nx_struct AseProtocolMsg {
  nx_uint8_t msg_type;
  nx_uint16_t node_id;
  nx_uint8_t weight;
  nx_uint8_t n_dependents;
} AseProtocolMsg;

typedef nx_struct AseSensorReadingMsg {
  nx_uint16_t source_node_id;
  nx_uint32_t timestamp;
  nx_uint8_t radiation_r;
  nx_uint8_t temperature_r;
  nx_uint8_t smoke_r;
} AseSensorReadingMsg;

enum {
  PROTOCOL_BROADCAST = 1,
  PROTOCOL_BROADCAST_ACKNOWLEDGEMENT = 2,
  PROTOCOL_CONFIRM = 3,
  ROOT_ACKNOWLEDGE = 4,
  ROOT_TIMERESET = 5,
  ASE_PROTOCOL_MSG = 255
};

#endif
