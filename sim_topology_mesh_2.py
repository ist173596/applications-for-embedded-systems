#! /usr/bin/python
from TOSSIM import *
from AseRootMsg import *
import sys

t = Tossim([])

r = t.radio()
f = open("config/topology_mesh_2.txt", "r")
for line in f:
  s = line.split()
  if s:
    print " ", s[0], " ", s[1], " ", s[2];
    r.add(int(s[0]), int(s[1]), float(s[2]))

noise = open("config/meyer-heavy.txt", "r")
for line in noise:
  str1 = line.strip()
  if str1:
    val = int(str1)
    for i in range(0, 17):
      t.getNode(i).addNoiseTraceReading(val)

for i in range(0, 17):
  print "Creating noise model for ",i;
  t.getNode(i).createNoiseModel()

t.addChannel("Log", sys.stdout)
t.addChannel("Error", sys.stdout);
f = open("read.txt", "w")
t.addChannel("Read", f);

t.getNode(0).bootAtTime(0)
t.getNode(1).bootAtTime(0)
t.getNode(2).bootAtTime(0)
t.getNode(3).bootAtTime(0)
t.getNode(4).bootAtTime(0)
t.getNode(5).bootAtTime(0)
t.getNode(6).bootAtTime(0)
t.getNode(7).bootAtTime(0)
t.getNode(8).bootAtTime(0)
t.getNode(9).bootAtTime(0)
t.getNode(10).bootAtTime(0)
t.getNode(11).bootAtTime(0)
t.getNode(12).bootAtTime(0)
t.getNode(13).bootAtTime(0)
t.getNode(14).bootAtTime(0)
t.getNode(15).bootAtTime(0)
t.getNode(16).bootAtTime(0)

for i in range(10000):
  t.runNextEvent()

msg = AseRootMsg()
msg.set_msg_type(5)
msg.set_newtime(4000)
msg.set_weight(0)
pkt = t.newPacket()
pkt.setData(msg.data)
pkt.setType(msg.get_amType())
pkt.setDestination(0)
pkt.deliverNow(0)

print("\nInjecting time set message: 4000ms! ---------------------------------------------------\n")

for i in range(10000):
  t.runNextEvent()

print("\nTurning node 2 Off! ---------------------------------------------------\n")
#print(t.getNode(6).isOn())
t.getNode(2).turnOff()
#print(t.getNode(6).isOn())

for i in range(40000):
  t.runNextEvent()

print("\nTurning node 2 ON! ---------------------------------------------------\n")

t.getNode(2).turnOn()

for i in range(40000):
  t.runNextEvent()
