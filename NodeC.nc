#include <Timer.h>
#include "ExternalFunction.h"
#include "AseProtocol.h"
#include "AseLinkedList.h"
#include "AseRootMsg.h"

module NodeC {
  uses interface Boot as Boot;
  uses interface Leds as StatusLeds; //status leds - led0 for boot
                                     //            - led1 for in-network
                                     //            - led2 for sending packets
  uses interface Timer<TMilli> as ProtocolTimer;
  uses interface Timer<TMilli> as SensorReadTimer;
  uses interface Timer<TMilli> as ReadingTimeoutTimer;
  uses interface AMSend as RadioOut;
  uses interface Receive as RadioIn;
  uses interface Read<uint16_t> as TempSensor;
  uses interface Read<uint16_t> as SmokeSensor;
  uses interface Read<uint16_t> as RadSensor;
  uses interface SplitControl as AMControl;
}
implementation {
  bool _busy = FALSE; //flag busy radio
  bool _busy_temp = FALSE; //flag busy temperature-sensor
  bool _busy_smoke = FALSE; //flag busy smoke-sensor
  bool _busy_rad = FALSE; //flag busy radiation-sensor

  message_t _protocol_packet, _sensor_reading_packet, _root_packet, _root_ack; //packets

  uint16_t _sensor_reading_timer_period = 3000; //changeable
  uint16_t _fast_protocol_timer_period = 5000; //time between broadcasts
  uint16_t _slow_protocol_timer_period = 10000; //time between broadcasts
  uint16_t _reading_timeout = 30000; //

  uint16_t _node_id; //id assigned to individual node
  uint8_t _weight; //weight (position) in network

  uint16_t _temp_seeker; //position in file
  uint16_t _smoke_seeker; //position in file
  uint16_t _rad_seeker; //position in file

  uint8_t _temp_val; //value of temperature read
  uint8_t _smoke_val; //value of smoke read
  uint8_t _rad_val; //value of radiation read

  //Node _dependents; //list for nodes that depend on this node
                    //to make a communication with the root.
  Node _depends_on; //list for nodes that this node depends to
                    //to make a communication with the root.
  Node tmp = NULL;

  event void Boot.booted() {
    call StatusLeds.led0On();
    call AMControl.start();
    _node_id = TOS_NODE_ID;
    if (_node_id == 0)
      _weight = 1;
    else
      _weight = 0;
    dbg("Log", "Info : %d : Machine %d : Boot.booted with weight %d\n"
      , time_now(), _node_id, _weight);
  }

  event void ReadingTimeoutTimer.fired() {
    //AseSensorReadingMsg *readingMsg;
    dbg("Log", "Info : %d : Machine %d : ReadingTimeout Fired!\n"
        , time_now(), _node_id);
    _depends_on = removeNode(_depends_on, _depends_on->node_id);
    if(_depends_on == NULL) 
    {
      _weight = 0;
      call ProtocolTimer.startPeriodic(_fast_protocol_timer_period);
      return;
    }
    /*
    readingMsg =
      (AseSensorReadingMsg*) (call RadioOut.getPayload(&_sensor_reading_packet, sizeof(AseSensorReadingMsg)));
    readingMsg->source_node_id = _node_id;
    readingMsg->timestamp = time_now();
    readingMsg->radiation_r = _rad_val;
    readingMsg->temperature_r = _temp_val;
    readingMsg->smoke_r = _smoke_val;

    if (call RadioOut.send(_depends_on->node_id, &_sensor_reading_packet, sizeof(AseSensorReadingMsg)) == SUCCESS) {
      _busy = TRUE;
      call ReadingTimeoutTimer.stop();
      call ReadingTimeoutTimer.startOneShot(_reading_timeout);
      dbg("Log", "Info : %d : Machine %d : Sent reading message to hop on machine %d.\n"
          , time_now(), _node_id, _depends_on->node_id);
    }*/
  }

  event void SensorReadTimer.fired() {
    if (!_busy_temp && !_busy_rad && !_busy_smoke) {
      if (_depends_on != NULL || _node_id == 0) {
        _busy_temp = TRUE;
        call TempSensor.read();

        _busy_rad = TRUE;
        call RadSensor.read();

        _busy_smoke = TRUE;
        call SmokeSensor.read();

        if (_node_id != 0) {
          AseSensorReadingMsg *readingMsg =
            (AseSensorReadingMsg*) (call RadioOut.getPayload(&_sensor_reading_packet, sizeof(AseSensorReadingMsg)));
          if(_depends_on == NULL) return;

          readingMsg->source_node_id = _node_id;
          readingMsg->timestamp = time_now();
          readingMsg->radiation_r = _rad_val;
          readingMsg->temperature_r = _temp_val;
          readingMsg->smoke_r = _smoke_val;

          if (call RadioOut.send(_depends_on->node_id, &_sensor_reading_packet, sizeof(AseSensorReadingMsg)) == SUCCESS) {
            _busy = TRUE;
            if(!(call ReadingTimeoutTimer.isRunning()))
              call ReadingTimeoutTimer.startOneShot(_reading_timeout);
            dbg("Log", "Info : %d : Machine %d : Sent reading message to hop on machine %d.\n"
                , time_now(), _node_id, _depends_on->node_id);
          }
        }
        else {
          if (_temp_val >= MAX_TEMP_ALERT || _smoke_val >= MAX_SMOKE_ALERT || _rad_val >= MAX_RAD_ALERT)
            dbg("Read", "WARNING : Node: %d : Timestamp: %d : Temperature: %d : Radiation: %d : Smoke: %d\n"
              , _node_id, time_now(), _temp_val, _rad_val, _smoke_val);
          else
            dbg("Read", "Reading : Node: %d : Timestamp: %d : Temperature: %d : Radiation: %d : Smoke: %d\n"
              , _node_id, time_now(), _temp_val, _rad_val, _smoke_val);
        }
      }
    }
  }

  event void TempSensor.readDone(error_t result, uint16_t val) {
    /*FILE *file = fopen("sensor-files/temp-vals-1000.txt", "r");
    uint16_t temp_value;
    fseek(file, _temp_seeker, SEEK_SET);
    fscanf(file, "%d", &_temp_val);
    fclose(file);*/
    _temp_val = _node_id;
    _temp_seeker += 3;
    _busy_temp = FALSE;
  }

  event void SmokeSensor.readDone(error_t result, uint16_t val) {
    /*FILE *file = fopen("sensor-files/smoke-vals-1000.txt", "r");
    uint8_t smoke_value;
    fseek(file, _smoke_seeker, SEEK_SET);
    fscanf(file, "%d", &_smoke_val);
    fclose(file);*/
    _smoke_val = _node_id;
    _smoke_seeker += 2;
    _busy_smoke = FALSE;
  }

  event void RadSensor.readDone(error_t result, uint16_t val) {
    /*FILE *file = fopen("sensor-files/rad-vals-1000.txt", "r");
    uint16_t rad_value;
    fseek(file, _rad_seeker, SEEK_SET);
    fscanf(file, "%d", &_rad_val);
    fclose(file);*/
    _rad_val = _node_id;
    _rad_seeker += 3;
    _busy_rad = FALSE;
  }

  event void ProtocolTimer.fired() {
    if (!_busy) {
      if (_node_id != 0) {
          AseProtocolMsg *broadcast =
            (AseProtocolMsg*) (call RadioOut.getPayload(&_protocol_packet, sizeof(AseProtocolMsg)));
          broadcast->msg_type = PROTOCOL_BROADCAST;
          broadcast->node_id = _node_id;

          if (call RadioOut.send(AM_BROADCAST_ADDR, &_protocol_packet, sizeof(AseProtocolMsg)) == SUCCESS) {
            _busy = TRUE;
            dbg("Log", "Info : %d : Machine %d : Protocol : Finding peers.\n"
                , time_now(), _node_id);
          }
      }
    }
  }

  event void RadioOut.sendDone(message_t *msg, error_t err) {
    if ((&_protocol_packet == msg 
      || &_sensor_reading_packet == msg 
      || &_root_ack == msg
      || &_root_packet == msg) && err != FAIL) {
      _busy = FALSE;
    } else {
      dbg("Error", "Error : %d : Machine %d : Message sending failed.\n"
        , time_now(), _node_id);
    }
  }

  event message_t* RadioIn.receive(message_t * msg, void *payload, uint8_t len) {
    Node tmpNode = NULL;
    if(len == sizeof(AseRootMsg)){

      AseRootMsg *reply = (AseRootMsg*) payload;
      uint8_t msgtype = reply->msg_type;

      if(msgtype == ROOT_ACKNOWLEDGE){
        if(_node_id == reply->destination_node_id){
          call ReadingTimeoutTimer.stop();
          dbg("Log", "Info : %d : Machine %d : Root Acknowledge received.\n"
            , time_now(), _node_id);
        }
        else{
          if (reply->weight < _weight) {
            AseRootMsg *rootMsg = (AseRootMsg*) (call RadioOut.getPayload(&_root_ack, sizeof(AseRootMsg)));
            rootMsg->msg_type = (int) ROOT_ACKNOWLEDGE;
            rootMsg->destination_node_id = reply->destination_node_id;
            rootMsg->weight = _weight;
            if(call RadioOut.send(AM_BROADCAST_ADDR, &_root_ack, sizeof(AseRootMsg))== SUCCESS){
              _busy = TRUE;
            }
          }
        }
      }

      else if(msgtype == ROOT_TIMERESET){
        uint16_t new_time = reply->newtime;
        _sensor_reading_timer_period = new_time;
        call SensorReadTimer.startPeriodic(_sensor_reading_timer_period);
        dbg("Log", "Info : %d : Machine %d : Received New Time %dms. ----------------------------------- New time\n"
                , time_now(), _node_id, new_time);

        if (reply->weight < _weight) {

          AseRootMsg *rootMsg = (AseRootMsg*) (call RadioOut.getPayload(&_root_packet, sizeof(AseRootMsg)));
          rootMsg->msg_type = (int) ROOT_TIMERESET;
          rootMsg->newtime = _sensor_reading_timer_period;
          rootMsg->weight = _weight;

          if(call RadioOut.send(AM_BROADCAST_ADDR, &_root_packet, sizeof(AseRootMsg)) == SUCCESS){
            _busy = TRUE;
          }
        }
      }

    }


    if (len == sizeof(AseSensorReadingMsg)) {
      AseSensorReadingMsg *reply = (AseSensorReadingMsg*)payload;

      if (_node_id != 0) {
        if (_depends_on != NULL) {
          AseSensorReadingMsg *readingMsg =
            (AseSensorReadingMsg*) (call RadioOut.getPayload(&_sensor_reading_packet, sizeof(AseSensorReadingMsg)));
          readingMsg->source_node_id = reply->source_node_id;
          readingMsg->timestamp = reply->timestamp;
          readingMsg->radiation_r = reply->smoke_r;
          readingMsg->temperature_r = reply->smoke_r;
          readingMsg->smoke_r = reply->smoke_r;

          if (call RadioOut.send(_depends_on->node_id, &_sensor_reading_packet, sizeof(AseSensorReadingMsg)) == SUCCESS) {
            _busy = TRUE;
            //dbg("Log", "Info : %d : Machine %d : Sent reading message from %d to hop on machine %d.\n"
            //  , time_now(), _node_id, readingMsg->source_node_id, _depends_on->node_id);
          }
        }
      }
      else {
        AseRootMsg *rootMsg = (AseRootMsg*) (call RadioOut.getPayload(&_root_ack, sizeof(AseRootMsg)));
        rootMsg->msg_type = (int) ROOT_ACKNOWLEDGE;
        rootMsg->destination_node_id = reply->source_node_id;

        if(call RadioOut.send(AM_BROADCAST_ADDR, &_root_ack, sizeof(AseRootMsg))== SUCCESS){
          _busy = TRUE;
        }

        //dbg("Log", "Info : %d : Machine %d : The message from %d reached the root.\n"
        //  , time_now(), _node_id, reply->source_node_id);

        if (reply->temperature_r >= MAX_TEMP_ALERT || reply->smoke_r >= MAX_SMOKE_ALERT || reply->radiation_r >= MAX_RAD_ALERT)
          dbg("Read", "WARNING : Node: %d : Timestamp: %d : Temperature: %d : Radiation: %d : Smoke: %d\n"
            , reply->source_node_id, reply->timestamp, reply->temperature_r
            , reply->radiation_r, reply->smoke_r);
        else
          dbg("Read", "Reading : Node: %d : Timestamp: %d : Temperature: %d : Radiation: %d : Smoke: %d\n"
            , reply->source_node_id, reply->timestamp, reply->temperature_r
            , reply->radiation_r, reply->smoke_r);
      }
    }
    /*
     * Workflow for protocol communication.
     * After a roundtrip of those messages it is supposed to
     * occur a connection between 2 close nodes.
     */
    else if (len == sizeof(AseProtocolMsg)) {
      AseProtocolMsg *packet = (AseProtocolMsg*)payload;

      if (packet->msg_type == (int)PROTOCOL_BROADCAST) {
        if (_weight != 0) {
          uint16_t sending_node = packet->node_id;
          AseProtocolMsg *ack =
            (AseProtocolMsg*) (call RadioOut.getPayload(&_protocol_packet, sizeof(AseProtocolMsg)));
          ack->msg_type = PROTOCOL_BROADCAST_ACKNOWLEDGEMENT;
          ack->node_id = _node_id;
          ack->weight = _weight;

          dbg("Log", "Info : %d : Machine %d : Received PROTOCOL_BROADCAST from Machine %d.\n"
            , time_now(), _node_id, sending_node);
          dbg("Log", "Info : %d : Machine %d : Sent PROTOCOL_BROADCAST_ACKNOWLEDGEMENT to Machine %d.\n"
            , time_now(), _node_id, sending_node);
          if (call RadioOut.send(sending_node, &_protocol_packet, sizeof(AseProtocolMsg)) == SUCCESS) {
            _busy = TRUE;
          }
        }
      } else if (packet->msg_type == (int)PROTOCOL_BROADCAST_ACKNOWLEDGEMENT) {

        Node node;

        uint16_t sending_node = packet->node_id;
        uint8_t sending_weight = packet->weight;
        AseProtocolMsg *conf;

        if (_weight == 0 || sending_weight+1 <= _weight) {
          _weight = sending_weight+1;
          for(node = _depends_on; node != NULL; node=node->next){
            dbg("Log", "Info : %d : Machine %d : Depends on %d with weight %d.\n"
              , time_now(), _node_id, node->node_id, node->weight);
          }
          _depends_on = insert(_depends_on, sending_node, sending_weight);

          for(node = _depends_on; node != NULL; node=node->next){
            dbg("Log", "Info : %d : Machine %d : Depends on %d.\n"
              , time_now(), _node_id, node->node_id);
          }

          dbg("Log", "Info : %d : Machine %d : Received PROTOCOL_BROADCAST_ACKNOWLEDGEMENT from Machine %d.\n"
          , time_now(), _node_id, sending_node);

          call ProtocolTimer.startPeriodic(_slow_protocol_timer_period);
        }
      }
    }
    return msg;
  }

  event void AMControl.startDone(error_t err) {
    if (err == SUCCESS) {
      call SensorReadTimer.startPeriodic(_sensor_reading_timer_period);
      call ProtocolTimer.startPeriodic(_fast_protocol_timer_period);
      dbg("Log", "Info : %d : Machine %d : SensorReadTimer.startPeriodic : %dms\n"
        , time_now(), _node_id, _sensor_reading_timer_period);
    } else {
      call AMControl.start();
    }
  }

  event void AMControl.stopDone(error_t err) { }
}
