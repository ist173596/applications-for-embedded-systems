#! /usr/bin/python
from TOSSIM import *
import sys

t = Tossim([])

t.addChannel("Log", sys.stdout)
t.addChannel("Error", sys.stdout);

t.getNode(0).bootAtTime(1)
t.getNode(1).bootAtTime(2)
t.getNode(2).bootAtTime(3)
t.getNode(3).bootAtTime(4)

for i in range(1000):
  t.runNextEvent()
