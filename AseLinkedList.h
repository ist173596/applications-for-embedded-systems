#ifndef ASELINKEDLIST_H
#define ASELINKEDLIST_H

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

typedef struct node_t {
  uint16_t node_id;
  uint8_t weight;
  struct node_t *next;
} *Node;

Node new(uint16_t node_id, uint8_t weight){
	Node n = (Node) malloc (sizeof(struct node_t));
	n->node_id = node_id;
	n->weight = weight;
	n->next = NULL;
	return n;
}

Node insert(Node head, uint16_t node_id, uint8_t weight){
  Node t1, t2;

  Node n = new(node_id, weight);

  if (head == NULL) return n; //Se esta vazio, insere na primeira posicao

  for(t1 = head,t2 = NULL; t1 != NULL; t2 = t1, t1 = t1->next){ //Vai verificar se ja existe na lista com esse node_id
    if(t1->node_id == node_id){ //Se encontra com esse node_id
      if(t1 == head){ //Se esta logo no inicio
        if(t1->weight > weight){ //E o peso do node diminuiu
          t1->weight = weight;  // Altera o valor do node
          break;
        }
        else  //Se o peso nao diminuiu, nao muda a lista
          return head;
      }
      else{ // Se nao esta no inicio
        if(t2->weight > weight){ //Se o peso diminuiu
          t2->next = t1->next; // Retira o node da lista
          break;
        }
        else{
          if(t1->weight > weight){
            t1 = t2;
            break;
          }
        }  //Se o peso nao diminuiu, nao muda a lista
        return head;
      }
    free(t1); //Limpa o node removido
    }
  }
  if(head==NULL) return n; // se limpamos a lista

  for(t1 = head,t2 = head->next; t1 != NULL; t1 = t2, t2 = t2->next){ //vai ver onde inserir o novo no
    if(t1->node_id == node_id){
      t1->weight = weight;
      return head;
    }
    if(t2==NULL){
      t1->next = n;
      return head;
    }
    if(t2->node_id == node_id){
      t2->weight = weight;
      return head;
    }
    if(t2->weight > weight){
          n->next = t2;
          t1->next = n;
          return head;
    }
  }
return head;
}

Node searchByNodeId(Node head, uint16_t node_id){
  Node t;
  for(t = head; t != NULL; t = t -> next){
		if(t->node_id == node_id){
			return t;
		}
	}
	return NULL;
}

Node removeNode(Node head, uint16_t node_id){
  Node t,n;
  if(node_id == 0) return head; // It should be impossible to remove node 0
  for(t = head,n = NULL; t != NULL; n = t, t = t->next){
		if(t->node_id == node_id){
			if(t == head)
				head = t->next;
			else
				n->next = t->next;
			free(t);
		}
	}
	return head;
}

Node removeFirst(Node head){
  Node t;
  //if(head->node_id == 0) return head; // It should be impossible to remove node 0
  if(head->next == NULL){
    free(head);
    return NULL;
  }
  else{
    t = head;
    head = head->next;
    free(t);
    return head;
  }
}
// The list functions are made to returning char* to better use in logs

char *listNode(Node node){ //maybe we don't want the \n at the end
  char *temp = (char*) malloc (25 * sizeof(char));
  sprintf(temp,"Node_id: %d, Weight: %d\n", node->node_id, node->weight);
  return temp;
}


char *listNodes(Node head){
  char *string = (char*) malloc(sizeof(char));
  Node t;
  for(t=head; t!=NULL; t=t->next){
    char *tmp1 = string, *tmp2 = listNode(t);
    string = (char*) realloc(string, 1 + strlen(tmp1) + strlen(tmp2));
    strcpy(string, tmp1);
    strcat(string, tmp2);
  //  free(tmp1);
    free(tmp2);
  }
  return string;
}

#endif
