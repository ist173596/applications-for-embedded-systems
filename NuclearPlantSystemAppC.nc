configuration NuclearPlantSystemAppC { }
implementation {
  components MainC as BootC;
  components LedsC as StatusLedsC;
  components NodeC as App;
  components new TimerMilliC() as ProtocolTimerC;
  components new TimerMilliC() as ReadingTimeoutTimerC;
  components new TimerMilliC() as SensorReadTimerC;
  components new SineSensorC() as TemperatureSensorC;
  components new SineSensorC() as SmokeSensorC;
  components new SineSensorC() as RadiationSensorC;
  components new AMSenderC(ASE_PROTOCOL_MSG) as SenderC;
  components new AMReceiverC(ASE_PROTOCOL_MSG) as ReceiverC;
  components ActiveMessageC as RadioC;

  App.Boot -> BootC;
  App.StatusLeds -> StatusLedsC;
  App.TempSensor -> TemperatureSensorC;
  App.SmokeSensor -> SmokeSensorC;
  App.RadSensor -> RadiationSensorC;
  App.ProtocolTimer -> ProtocolTimerC;
  App.SensorReadTimer -> SensorReadTimerC;
  App.ReadingTimeoutTimer -> ReadingTimeoutTimerC;
  App.RadioOut -> SenderC;
  App.RadioIn -> ReceiverC;
  App.AMControl -> RadioC;
}
