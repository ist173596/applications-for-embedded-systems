#
# This class is automatically generated by mig. DO NOT EDIT THIS FILE.
# This class implements a Python interface to the 'AseRootMsg'
# message type.
#

import tinyos.message.Message

# The default size of this message type in bytes.
DEFAULT_MESSAGE_SIZE = 6

# The Active Message type associated with this message.
AM_TYPE = -1

class AseRootMsg(tinyos.message.Message.Message):
    # Create a new AseRootMsg of size 6.
    def __init__(self, data="", addr=None, gid=None, base_offset=0, data_length=6):
        tinyos.message.Message.Message.__init__(self, data, addr, gid, base_offset, data_length)
        self.amTypeSet(AM_TYPE)
    
    # Get AM_TYPE
    def get_amType(cls):
        return AM_TYPE
    
    get_amType = classmethod(get_amType)
    
    #
    # Return a String representation of this message. Includes the
    # message type name and the non-indexed field values.
    #
    def __str__(self):
        s = "Message <AseRootMsg> \n"
        try:
            s += "  [msg_type=0x%x]\n" % (self.get_msg_type())
        except:
            pass
        try:
            s += "  [newtime=0x%x]\n" % (self.get_newtime())
        except:
            pass
        try:
            s += "  [destination_node_id=0x%x]\n" % (self.get_destination_node_id())
        except:
            pass
        try:
            s += "  [weight=0x%x]\n" % (self.get_weight())
        except:
            pass
        return s

    # Message-type-specific access methods appear below.

    #
    # Accessor methods for field: msg_type
    #   Field type: short
    #   Offset (bits): 0
    #   Size (bits): 8
    #

    #
    # Return whether the field 'msg_type' is signed (False).
    #
    def isSigned_msg_type(self):
        return False
    
    #
    # Return whether the field 'msg_type' is an array (False).
    #
    def isArray_msg_type(self):
        return False
    
    #
    # Return the offset (in bytes) of the field 'msg_type'
    #
    def offset_msg_type(self):
        return (0 / 8)
    
    #
    # Return the offset (in bits) of the field 'msg_type'
    #
    def offsetBits_msg_type(self):
        return 0
    
    #
    # Return the value (as a short) of the field 'msg_type'
    #
    def get_msg_type(self):
        return self.getUIntElement(self.offsetBits_msg_type(), 8, 1)
    
    #
    # Set the value of the field 'msg_type'
    #
    def set_msg_type(self, value):
        self.setUIntElement(self.offsetBits_msg_type(), 8, value, 1)
    
    #
    # Return the size, in bytes, of the field 'msg_type'
    #
    def size_msg_type(self):
        return (8 / 8)
    
    #
    # Return the size, in bits, of the field 'msg_type'
    #
    def sizeBits_msg_type(self):
        return 8
    
    #
    # Accessor methods for field: newtime
    #   Field type: int
    #   Offset (bits): 8
    #   Size (bits): 16
    #

    #
    # Return whether the field 'newtime' is signed (False).
    #
    def isSigned_newtime(self):
        return False
    
    #
    # Return whether the field 'newtime' is an array (False).
    #
    def isArray_newtime(self):
        return False
    
    #
    # Return the offset (in bytes) of the field 'newtime'
    #
    def offset_newtime(self):
        return (8 / 8)
    
    #
    # Return the offset (in bits) of the field 'newtime'
    #
    def offsetBits_newtime(self):
        return 8
    
    #
    # Return the value (as a int) of the field 'newtime'
    #
    def get_newtime(self):
        return self.getUIntElement(self.offsetBits_newtime(), 16, 1)
    
    #
    # Set the value of the field 'newtime'
    #
    def set_newtime(self, value):
        self.setUIntElement(self.offsetBits_newtime(), 16, value, 1)
    
    #
    # Return the size, in bytes, of the field 'newtime'
    #
    def size_newtime(self):
        return (16 / 8)
    
    #
    # Return the size, in bits, of the field 'newtime'
    #
    def sizeBits_newtime(self):
        return 16
    
    #
    # Accessor methods for field: destination_node_id
    #   Field type: int
    #   Offset (bits): 24
    #   Size (bits): 16
    #

    #
    # Return whether the field 'destination_node_id' is signed (False).
    #
    def isSigned_destination_node_id(self):
        return False
    
    #
    # Return whether the field 'destination_node_id' is an array (False).
    #
    def isArray_destination_node_id(self):
        return False
    
    #
    # Return the offset (in bytes) of the field 'destination_node_id'
    #
    def offset_destination_node_id(self):
        return (24 / 8)
    
    #
    # Return the offset (in bits) of the field 'destination_node_id'
    #
    def offsetBits_destination_node_id(self):
        return 24
    
    #
    # Return the value (as a int) of the field 'destination_node_id'
    #
    def get_destination_node_id(self):
        return self.getUIntElement(self.offsetBits_destination_node_id(), 16, 1)
    
    #
    # Set the value of the field 'destination_node_id'
    #
    def set_destination_node_id(self, value):
        self.setUIntElement(self.offsetBits_destination_node_id(), 16, value, 1)
    
    #
    # Return the size, in bytes, of the field 'destination_node_id'
    #
    def size_destination_node_id(self):
        return (16 / 8)
    
    #
    # Return the size, in bits, of the field 'destination_node_id'
    #
    def sizeBits_destination_node_id(self):
        return 16
    
    #
    # Accessor methods for field: weight
    #   Field type: short
    #   Offset (bits): 40
    #   Size (bits): 8
    #

    #
    # Return whether the field 'weight' is signed (False).
    #
    def isSigned_weight(self):
        return False
    
    #
    # Return whether the field 'weight' is an array (False).
    #
    def isArray_weight(self):
        return False
    
    #
    # Return the offset (in bytes) of the field 'weight'
    #
    def offset_weight(self):
        return (40 / 8)
    
    #
    # Return the offset (in bits) of the field 'weight'
    #
    def offsetBits_weight(self):
        return 40
    
    #
    # Return the value (as a short) of the field 'weight'
    #
    def get_weight(self):
        return self.getUIntElement(self.offsetBits_weight(), 8, 1)
    
    #
    # Set the value of the field 'weight'
    #
    def set_weight(self, value):
        self.setUIntElement(self.offsetBits_weight(), 8, value, 1)
    
    #
    # Return the size, in bytes, of the field 'weight'
    #
    def size_weight(self):
        return (8 / 8)
    
    #
    # Return the size, in bits, of the field 'weight'
    #
    def sizeBits_weight(self):
        return 8
    
