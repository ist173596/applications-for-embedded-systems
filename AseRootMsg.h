#ifndef ASEROOTMSG
#define ASEROOTMSG

typedef nx_struct AseRootMsg {
  nx_uint8_t msg_type;
  nx_uint16_t newtime;
  nx_uint16_t destination_node_id;
  nx_uint8_t weight;
} AseRootMsg;

#endif